var gulp            = require('gulp'),
    babelify        = require('babelify'),
    browserify      = require('browserify'),
    ngAnnotate      = require('browserify-ngannotate'),
    ngConstant      = require('gulp-ng-constant'),
    source          = require('vinyl-source-stream'),
    browserSync     = require('browser-sync').create(),
    modRewrite      = require('connect-modrewrite'),
    notify          = require('gulp-notify'),
    rename          = require('gulp-rename'),
    jsonSass        = require('gulp-json-sass'),
    fs              = require('fs'),
    autoprefixer    = require('gulp-autoprefixer'),
    sass            = require('gulp-sass'),
    templateCache   = require('gulp-angular-templatecache'),
    argv            = require('yargs').argv,
    concat          = require('gulp-concat'),
    minifyCSS       = require('gulp-cssnano'),
    merge           = require('merge-stream'),
    Server 	    = require('karma').Server,
    uglify          = require('gulp-uglify');

/* Where our files are located. */
var jsFiles     = [ 'src/**/*.js', '!src/config/app.constants.js', '!src/config/app.templates.js' ];
var scssFiles   = [ 'src/**/*.scss', '!src/styles/config.scss' ];
var indexFile   = 'src/index.html';
var viewFiles   = 'src/**/*.html';
var envFiles    = 'envs/**/*.*';

/* Env file */
var env = 'envs/' + ( argv.env || 'local' ) + '.json';

/* Default task. This will be run when no task is passed in arguments to gulp. */
gulp.task('default', [ 'css', 'js', 'html' ]);

/* Move index file into build. */
gulp.task('html', function () {
    return gulp.src('./src/index.html')
        .on('error', interceptErrors)
        .pipe(gulp.dest('./build/'));
});

/* Build views into angular template cache. */
gulp.task('views', [ 'jsConfig' ], function () {
    return gulp.src(viewFiles)
        .pipe(templateCache({ standalone: true }))
        .on('error', interceptErrors)
        .pipe(rename('app.templates.js'))
        .pipe(gulp.dest('./src/config/'));
});

gulp.task('jsConfig', function () {
    return gulp.src(env)
        .pipe(ngConstant({ name: "app.constants" }))
        .pipe(rename('app.constants.js'))
        .pipe(gulp.dest('src/config'));
});

/* Convert ES6 code in all js files in src/js folder and copy to build folder > app.js. */
gulp.task('js', [ 'views' ], function () {
    return browserify({ entries: [ './src/app.js' ] })
        .transform(babelify.configure({ presets : [ 'es2015' ] }))
        .transform(ngAnnotate)
        .bundle()
        .on('error', interceptErrors)
        .pipe(source('app.js'))
        .pipe(gulp.dest('./build'));
});

/* Build & compile css files from css folder to build folder > app.css. */
gulp.task('css', function () {
    return gulp.src('src/app.scss')
        .pipe(sass({ outputStyle: 'compressed' }))
        .pipe(concat('app.css'))
        .pipe(autoprefixer())
        .pipe(minifyCSS())
        .on('error', interceptErrors)
        .pipe(gulp.dest('./build'));
});

gulp.task('test', [ 'css', 'js', 'html' ], function( done ) {
    new Server({
        configFile: __dirname + '/karma.conf.js'
    }, done).start();
});

/* Build production ready minified JS/CSS files into the dist/ folder. */
gulp.task('deploy', [ 'css', 'js', 'html' ], function() {
    var html = gulp.src('build/index.html')
        .pipe(gulp.dest('./dist/'));

    var js = gulp.src('build/app.js')
        .pipe(uglify())
        .pipe(gulp.dest('./dist/'));

    var css = gulp.src('build/app.css')
        .pipe(gulp.dest('./dist/'));

    return merge( html, js, css );
});

/* Dev task. */
gulp.task('dev', [ 'css', 'js', 'html' ], function () {
    browserSync.init(['./build/**/**.**'], {
        server: {
            baseDir: "./build",
            middleware: [
                modRewrite(['!\.html|\.js|\.jpg|\.mp4|\.mp3|\.gif|\.svg|\.css|\.ttf|\.eot|\.otf|\.woff|\.woff2|\.png$ /index.html [L]'])
            ]
        },
        cors: true,
        port: 4008,
        notify: true,
        ui: {
            port: 4009
        }
    });

    /* Watchers */
    gulp.watch(jsFiles, [ 'js' ]);
    gulp.watch(viewFiles, [ 'js' ]);
    gulp.watch(scssFiles, [ 'css' ]);
    gulp.watch(indexFile, [ 'html' ]);
    gulp.watch(envFiles, [ 'js' ]);
});

/* Error Interceptor */
var interceptErrors = function(error) {
    var args = Array.prototype.slice.call(arguments);

    // Send error to notification center with gulp-notify
    notify.onError({
        title: 'Compile Error',
        message: '<%= error.message %>'
    }).apply(this, args);

    /* Keep gulp from hanging on this task */
    this.emit('end');
};
