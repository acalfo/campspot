describe('Reservations', function() {

    beforeEach(angular.mock.module('app'));

    beforeEach( inject( _Reservations_ => {
        this.Reservations = _Reservations_;

        this.testReservations = {
            '2016-06-11': {"campsiteId": 1,  "startDate": "2016-06-11",  "endDate": "2016-06-13" },
            '2016-06-12': {"campsiteId": 1,  "startDate": "2016-06-11",  "endDate": "2016-06-13" },
            '2016-06-13': {"campsiteId": 1,  "startDate": "2016-06-11",  "endDate": "2016-06-13" }
        };
    } ));

    it('Should be defined.', () => expect(this.Reservations).toBeDefined() );

    it('Should determine a reservation already exists for an invalid date range.', () => {
        let startDate =  new Date(2016, 5, 12);
        let endDate = new Date(2016, 5, 13);

        expect(this.Reservations.isReserved(this.testReservations, startDate, endDate)).toBeTruthy();
    });

    it('Should determine a reservation doesn\'t exist for an valid date range.', () => {
        let startDate = new Date(2016, 5, 3);
        let endDate = new Date(2016, 5, 10);

        expect(this.Reservations.isReserved(this.testReservations, startDate, endDate)).toBeFalsy();
    });

    it('Should determine a gap size is too large for an invalid date range.', () => {
        let startDate = new Date(2016, 5, 5);
        let endDate = new Date(2016, 5, 8);

        expect(this.Reservations.isGap(this.testReservations, startDate, endDate)).toBeTruthy();
    });

    it('Should determine a gap size is not too large for an valid date range.', () => {
        let startDate = new Date(2016, 5, 5);
        let endDate = new Date(2016, 5, 9);

        expect(this.Reservations.isGap(this.testReservations, startDate, endDate)).toBeFalsy();
    });

    it('Should return the answer.', () => {
        let result = this.Reservations.getAvailableCampsites( '2016-06-07', '2016-06-10' );
        let answerIds = [ 5, 6, 8, 9];

        R.forEach( campsite => expect(R.contains(campsite.id, answerIds)).toBeTruthy(), result );
        expect(result.length).toEqual(answerIds.length);
    });
});