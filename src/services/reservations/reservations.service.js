export default class Reservations {
    constructor( SampleData, Helpers ) {
        'ngInject';

        /* Could get gap sizes from DOM trivially */
        this.gapSizes = SampleData.gapSizes;

        /* Could get campsites from api trivally */
        this.campsites = SampleData.getCampsitesWithReservations();

        /* Attach helper date functions */
        this.formatDate = Helpers.formatDate;
        this.unformatDate = Helpers.unformatDate;
    }

    /*
     * Gets the list of campsites that are available for the given date range.
     *
     * @param startDate ( string ) The day the reservation would begin.
     * @param startDate ( string ) The day the reservation would begin.
     *
     * @return campites ( array ) The array of available campsites for the specified date range.
     *
     */
    getAvailableCampsites( startDate, endDate ) {
        return R.filter( this.isAvailable( this.unformatDate( startDate ), this.unformatDate( endDate ) ), this.campsites);
    }


    /*
     * Checks if a campsite is available for the given date range
     *
     * @param startDate ( date )
     * @param startDate ( date )
     *
     * @return fn that evaluates to boolean
     */
    isAvailable( startDate, endDate ) {
            return campsite => {
                return ! this.isReserved( campsite.reservations, R.clone(startDate), R.clone(endDate) ) &&
                       ! this.isGap( campsite.reservations, startDate, endDate );
            }
    }

    /*
     * Checks if a campsite is reserved for the given date range
     */
    isReserved( reservations, startDate, endDate ) {
        do {
            /* Check if reservation exists in campsite's reservation map */
            if ( reservations[ this.formatDate( startDate ) ] ){
                return true;
            }
            /* Increment Date */
            startDate.setDate( startDate.getDate() + 1 );

        } while ( startDate <= endDate )
    }

    /*
     * Checks if a new campsite reservation would create a gap too large defined by the given gap sizes.
     */
    isGap( reservations, startDate, endDate ) {
        let counter, prevGap, nextGap, next, prev;

        for ( var i = 0; i < this.gapSizes.length; i++ ) {
            next = R.clone(endDate),
            prev = R.clone(startDate),

            counter = this.gapSizes[i] + 1,
            prevGap = nextGap = 0;

            do {
                /* Increment dates by a day. */
                next.setDate(next.getDate() + 1);
                prev.setDate(prev.getDate() - 1);

                /* Increment gap count if reservation doesn't exist */
                if ( ! reservations[ this.formatDate(prev) ] ) {
                    prevGap++;

                /* Check gap size once a reservation is found */
                }  else if ( prevGap == this.gapSizes[i] ) {
                    return true;
                }

                if ( ! reservations[ this.formatDate(next) ] ) {
                    nextGap++;
                } else if ( nextGap == this.gapSizes[i] ) {
                    return true;
                }

            } while( counter-- )
        }
    }
}