import angular from 'angular';

let servicesModule = angular.module('app.services', []);

import ReservationsService from './reservations/reservations.service';
servicesModule.service('Reservations', ReservationsService);

import SampleDataService from './sampleData/sampleData.service';
servicesModule.service('SampleData', SampleDataService);

import HelpersService from './helpers/helpers.service';
servicesModule.service('Helpers', HelpersService);

export default servicesModule;