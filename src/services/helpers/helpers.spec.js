describe('Helpers', function() {

    beforeEach(angular.mock.module('app'));

    beforeEach( inject( _Helpers_ => {
        this.Helpers = _Helpers_;

        this.formattedDate = '2017-03-30';
        this.unformattedDate = new Date(2017, 2, 30);
    }));

    it('Should be defined.', () => expect(this.Helpers).toBeDefined() );

    it('Should format a date to a string.',
        () => expect( typeof(this.Helpers.formatDate( this.unformattedDate )) ).toBe( 'string' )
    );

    it('Should format a date to the correct string.',
        () => expect( this.Helpers.formatDate( this.unformattedDate )).toBe( this.formattedDate )
    );

    it('Should format a string to a date.',
        () => expect( this.Helpers.unformatDate( this.formattedDate ) instanceof Date ).toBeTruthy()
    );

    it('Should format a string to the correct day.',
        () => expect( this.Helpers.unformatDate( this.formattedDate ).getDate() ).toEqual( 30 )
    );

    it('Should format a string to the correct month.',
        () => expect( this.Helpers.unformatDate( this.formattedDate ).getMonth() ).toEqual( 2 )
    );

    it('Should format a string to the correct year.',
        () => expect( this.Helpers.unformatDate( this.formattedDate ).getFullYear() ).toEqual( 2017 )
    );
});