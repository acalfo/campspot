export default class Helpers {

    formatDate( date ) {
        return date.toISOString().slice(0,10).replace(/-/g,"-");
    }

    unformatDate( date ) {
        let d = R.split('-', date);
        return new Date( d[0], parseInt(d[1]) - 1, d[2] );
    }
}