export default class SampleData {
    constructor( Helpers ) {
        'ngInject';

        this.gapSizes = [ 2, 3 ];

        this.search = {
            "startDate": "2016-06-07",
            "endDate": "2016-06-10"
        };

        /* Attach helper date functions */
        this.formatDate = Helpers.formatDate;
        this.unformatDate = Helpers.unformatDate;
    }

    getCampsites() {
        return [{
            "id": 1,
            "name": "Grizzly Adams Adventure Cabin"
        }, {
            "id": 2,
            "name": "Lewis and Clark Camp Spot"
        }, {
            "id": 3,
            "name": "Jonny Appleseed Log Cabin"
        }, {
            "id": 4,
            "name": "Davey Crockett Camphouse"
        }, {
            "id": 5,
            "name": "Daniel Boone Bungalow"
        }, {
            "id": 6,
            "name": "Teddy Rosevelt Tent Site"
        }, {
            "id": 7,
            "name": "Edmund Hillary Igloo"
        }, {
            "id": 8,
            "name": "Bear Grylls Cozy Cave"
        }, {
            "id": 9,
            "name": "Wyatt Earp Corral"
        }];
    }

    getReservations() {
        return [
            {"campsiteId": 1, "startDate": "2016-06-01", "endDate": "2016-06-04"},
            {"campsiteId": 1, "startDate": "2016-06-11", "endDate": "2016-06-13"},
            {"campsiteId": 2, "startDate": "2016-06-08", "endDate": "2016-06-09"},
            {"campsiteId": 3, "startDate": "2016-06-04", "endDate": "2016-06-06"},
            {"campsiteId": 3, "startDate": "2016-06-14", "endDate": "2016-06-16"},
            {"campsiteId": 4, "startDate": "2016-06-03", "endDate": "2016-06-05"},
            {"campsiteId": 4, "startDate": "2016-06-13", "endDate": "2016-06-14"},
            {"campsiteId": 5, "startDate": "2016-06-03", "endDate": "2016-06-06"},
            {"campsiteId": 5, "startDate": "2016-06-12", "endDate": "2016-06-14"},
            {"campsiteId": 6, "startDate": "2016-06-04", "endDate": "2016-06-06"},
            {"campsiteId": 6, "startDate": "2016-06-11", "endDate": "2016-06-12"},
            {"campsiteId": 6, "startDate": "2016-06-16", "endDate": "2016-06-16"},
            {"campsiteId": 7, "startDate": "2016-06-03", "endDate": "2016-06-04"},
            {"campsiteId": 7, "startDate": "2016-06-07", "endDate": "2016-06-09"},
            {"campsiteId": 7, "startDate": "2016-06-13", "endDate": "2016-06-16"},
            {"campsiteId": 8, "startDate": "2016-06-01", "endDate": "2016-06-02"},
            {"campsiteId": 8, "startDate": "2016-06-05", "endDate": "2016-06-06"},
            {"campsiteId": 9, "startDate": "2016-06-03", "endDate": "2016-06-05"},
            {"campsiteId": 9, "startDate": "2016-06-12", "endDate": "2016-06-16"}
        ]
    }

    /* Could make this an API method. */
    getCampsitesWithReservations() {
        return R.map( this.assignReservations( this.getReservations() ), this.getCampsites() );
    }

    /* Creates the reservations map for each campsite */
    assignReservations( reservations ) {
        return campsite => {
            R.forEach( this.makeReservationMap(campsite), this.getReservationsByCampsite( campsite.id, reservations ));

            return campsite;
        }
    }

    /* Filter reservations by campsiteId */
    getReservationsByCampsite( campsiteId, reservations ) {
        return R.filter(R.propEq('campsiteId', campsiteId), reservations)
    }

    /* Create date string map on campsite's reservations array for each day of the reservation */
    makeReservationMap( campsite ) {
        campsite.reservations = {};

        return reservation => {
            let startDate = this.unformatDate(reservation.startDate);
            let endDate = this.unformatDate(reservation.endDate);

            do {
                campsite.reservations[ this.formatDate(startDate) ] = reservation;
                startDate.setDate( startDate.getDate() + 1);
            } while ( startDate <= endDate )
        }
    }
}