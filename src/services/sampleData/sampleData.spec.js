describe('Sample Data', function() {

    beforeEach(angular.mock.module('app'));

    beforeEach( inject( _SampleData_ => {
        this.SampleData = _SampleData_
        this.reservations = this.SampleData.getReservations();
        this.campsites = this.SampleData.getCampsites();
    }));

    it('Should be defined.', () => expect(this.SampleData).toBeDefined() );

    it('Should get the campsites array.', () => {
        expect(R.type(Array, this.campsites)).toBeTruthy();
        expect( this.campsites.length ).toBeGreaterThan(0);
    });

    it('Should get the reservations array.', () => {
        expect( R.type(Array, this.reservations) ).toBeTruthy();
        expect( this.reservations.length ).toBeGreaterThan(0);
    });

    it('Should get reservations by campsite correctly.', () => {
        let reservations = this.SampleData.getReservationsByCampsite(1, this.reservations );

        expect( reservations.length ).toEqual( 2 );
        R.forEach( reservation  => expect(reservation.campsiteId).toEqual(1), reservations)
    });

    it('Should create the reservation map correctly.', () => {
        let campsite = { "id": 1,  "name": "Grizzly Adams Adventure Cabin" };
        let reservation = {"campsiteId": 1, "startDate": "2016-06-01", "endDate": "2016-06-04"};

        /* Create Reservation Map on campsite */
        this.SampleData.makeReservationMap( campsite )( reservation );

        expect(campsite.reservations['2016-05-31']).toBe( undefined );
        expect(campsite.reservations['2016-06-01']).toBeDefined();
        expect(campsite.reservations['2016-06-02']).toBeDefined();
        expect(campsite.reservations['2016-06-03']).toBeDefined();
        expect(campsite.reservations['2016-06-04']).toBeDefined();
        expect(campsite.reservations['2016-06-05']).toBe( undefined );
    })

    it('Should return a reservations object with each campsite.', () => {
        expect( R.all(R.propIs(Object, 'reservations'), this.SampleData.getCampsitesWithReservations()) ).toBeTruthy();
    });

    it('Should build the reservations map correctly for each campsite', () => {
        let campsites = [{ "id": 1,  "name": "Grizzly Adams Adventure Cabin" }, { "id": 2,  "name": "Lewis and Clark Camp Spot" }]
        let reservations = [{"campsiteId": 1, "startDate": "2016-06-01", "endDate": "2016-06-04"},
                            {"campsiteId": 1, "startDate": "2016-06-11", "endDate": "2016-06-13"},
                            {"campsiteId": 2, "startDate": "2016-06-08", "endDate": "2016-06-09"}];

        let campsiteWithReservations = R.map( this.SampleData.assignReservations( reservations ), campsites );

        expect(campsiteWithReservations[0].reservations["2016-05-31"]).toBe( undefined );
        expect(campsiteWithReservations[0].reservations["2016-06-01"]).toBeDefined();
        expect(campsiteWithReservations[0].reservations["2016-06-02"]).toBeDefined();
        expect(campsiteWithReservations[0].reservations["2016-06-03"]).toBeDefined();
        expect(campsiteWithReservations[0].reservations["2016-06-04"]).toBeDefined();
        expect(campsiteWithReservations[0].reservations["2016-06-05"]).toBe( undefined );
        expect(campsiteWithReservations[0].reservations["2016-06-10"]).toBe( undefined );
        expect(campsiteWithReservations[0].reservations["2016-06-11"]).toBeDefined();
        expect(campsiteWithReservations[0].reservations["2016-06-12"]).toBeDefined();
        expect(campsiteWithReservations[0].reservations["2016-06-13"]).toBeDefined();
        expect(campsiteWithReservations[0].reservations["2016-06-14"]).toBe( undefined );

        expect(campsiteWithReservations[1].reservations["2016-06-07"]).toBe( undefined );
        expect(campsiteWithReservations[1].reservations["2016-06-08"]).toBeDefined();
        expect(campsiteWithReservations[1].reservations["2016-06-09"]).toBeDefined();
        expect(campsiteWithReservations[1].reservations["2016-06-10"]).toBe( undefined );
    })
});