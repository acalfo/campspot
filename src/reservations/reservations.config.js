function ReservationsConfig($stateProvider) {
    'ngInject';

    $stateProvider
        .state('reservations', {
            url: '/',
            controller: 'ReservationsCtrl',
            controllerAs: '$ctrl',
            templateUrl: 'reservations/reservations.html',
            title: 'Reservations'
        });
};

export default ReservationsConfig;