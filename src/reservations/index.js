import angular from 'angular';

/* Create the module where our functionality can attach to */
let reservationsModule = angular.module('app.reservations', []);

// Include our UI-Router config settings
import ReservationsConfig from './reservations.config';
reservationsModule.config(ReservationsConfig);

// Controllers
import ReservationsCtrl from './reservations.controller';
reservationsModule.controller('ReservationsCtrl', ReservationsCtrl);

export default reservationsModule;
