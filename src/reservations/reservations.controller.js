class ReservationsCtrl {
    constructor( Reservations, SampleData ) {
        'ngInject';

        this.Reservations = Reservations;

        this.startDate = SampleData.search.startDate;
        this.endDate = SampleData.search.endDate;

        /* Get reservations for sample data on boot */
        this.getCampsites( this.startDate, this.endDate );
    }

    getCampsites( startDate, endDate ) {
    	this.campsites = this.Reservations.getAvailableCampsites( startDate, endDate )
    }
}

export default ReservationsCtrl;