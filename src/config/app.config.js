function AppConfig( $locationProvider, $urlRouterProvider ){
    'ngInject';

    $locationProvider.html5Mode({ enabled: true, requireBase: false });

    $urlRouterProvider.otherwise('/');
}
export default AppConfig;