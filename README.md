#Campspot Programming Challenge 

By Alex Calfo

##Installation Instructions

Npm Gulp and Karma are required bin dependencies. If they are not installed

### Install NPM 
Go to https://nodejs.org/en/ and download the most recent version

### Install Gulp
`sudo npm install -g gulp`

### Install Karma
`sudo npm install -g karma-cli`

##How to run
At the root of the project directory run the following commands:

Install dependencies - `npm install`

Run the local dev server - `npm start`


## How to test
run `npm test`

Click the debug button in the top right corner of the chrome window that opens