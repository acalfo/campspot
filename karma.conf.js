module.exports = function(config) {
    config.set({
        basePath: '',
        browsers: ['Chrome'],
        frameworks: ['browserify', 'jasmine'],
        autoWatch: false,
        singleRun: false,
        preprocessors: {
            './src/**/*.js': ['babel', 'browserify'],
            './src/**/*.html': ['ng-html2js']
        },

        files: [
            './src/app.js',
            './node_modules/angular-mocks/angular-mocks.js',
            './src/**/*.html',
            './src/**/*.spec.js'
        ],

        ngHtml2JsPreprocessor: {
            moduleName: 'templates'
        },

        browserify: {
            configure: function browserify(bundle) {
                bundle.once('prebundle', function prebundle() {
                    bundle.transform('babelify', {presets: ['es2015']});
                    bundle.transform('browserify-ngannotate');
                });
            }
        },

        reporters: [ 'kjhtml' ],
    });
};
